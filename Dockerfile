FROM rocker/tidyverse:latest

RUN apt-get update -qq && apt-get install \
    curl \
    autoconf \
    automake \
    make \
    wget \
    libtool \
    pkg-config \
    build-essential \
    checkinstall \
    zlib1g-dev -y

# install libpostal
RUN git clone https://github.com/openvenues/libpostal

WORKDIR /libpostal

RUN ./bootstrap.sh &&\
./configure &&\
make -j8 &&\
make install &&\
ldconfig


RUN Rscript -e \
    "install.packages(c('tidymodels', 'knitr', 'kableExtra', 'plotly'))"

RUN Rscript -e \
    "devtools::install_github('ironholds/poster')"
